var map;
var infowindow;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center : {
			lat : null,
			lng : null
		},
		zoom : 13
	});
	var myLatLng = {
		lat : 21.027052,
		lng : 105.831394
	};

	var infoW = new google.maps.InfoWindow({
		map : map
	});

	// Try HTML5 geolocation.
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat : position.coords.latitude,
				lng : position.coords.longitude
			};
			infoW.setPosition(pos);
			infoW.setContent('Vị trí của bạn');
			map.setCenter(pos);
			infowindow = new google.maps.InfoWindow();
			var request = [];
			var listSearch = [ 'bún', 'cafe', 'coffe', 'bò', 'gà', 'phở',
					'sữa chua', 'chả', 'miến', 'cháo', 'cơm', 'nước',
					'giải khát' ];
			for (var i = 0; i < listSearch.length; i++) {
				var rq = {
					location : map.getCenter(),
					radius : '1',
					query : listSearch[i]
				};
				request.push(rq);
			}

			var service = new google.maps.places.PlacesService(map);
			for (var i = 0; i < request.length; i++) {
				service.textSearch(request[i], callback);
			}

		}, function() {
			handleLocationError(true, infoW, map.getCenter());
		});
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoW, map.getCenter());
	}

}

function handleLocationError(browserHasGeolocation, infoWi, pos) {
	infoW.setPosition(pos);
	infoW
			.setContent(browserHasGeolocation ? 'Error: The Geolocation service failed.'
					: 'Error: Your browser doesn\'t support geolocation.');
}
function callback(results, status) {
	if (status === google.maps.places.PlacesServiceStatus.OK) {
		for (var i = 0; i < results.length; i++) {
			createMarker(results[i]);
		}
	}
}

function createMarker(place) {
	var placeLoc = place.geometry.location;
	var photos = place.photos;
	if (!photos)
		return;
	var image = photos[0].getUrl({
		'maxWidth' : 120,
		'maxHeight' : 120
	})
	var icon = {
		url : place.icon,
		scaledSize : new google.maps.Size(25, 25),
	};

	var marker = new google.maps.Marker({
		map : map,
		animation : google.maps.Animation.DROP,
		position : place.geometry.location,
		icon : icon
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent('<div><img src=' + image + '><br><strong>'
				+ place.name + '</strong><br>' + place.formatted_address
				+ '</div>');
		infowindow.open(map, this);
	});
}
