<%@page import="java.time.LocalDateTime"%>
<%@page import="com.food.table.Pin"%>
<%@page import="com.food.address.AddressQuery"%>
<%@page import="com.food.address.Address"%>
<%@page import="com.food.query.SocialQuery"%>
<%@page import="java.util.Date"%>
<%@page import="com.food.user.UserQuery"%>
<%@page import="com.food.user.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.food.table.Comment"%>
<%@page import="com.food.query.FoodQuery"%>
<%@page import="com.food.table.Food"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ăn gì ?</title>
<style type="text/css">
.well {
	margin-top: -20px;
	background-color: #007FBD;
	border: 2px solid #0077B2;
	text-align: center;
	cursor: pointer;
	font-size: 25px;
	padding: 15px;
	border-radius: 0px !important;
}

body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 14px;
	line-height: 1.42857143;
	color: #fff;
	background-color: #F1F1F1;
}

.bg_blur {
	height: 300px;
	background-size: cover;
}

.panel {
	position: relative;
}

.panel>.panel-heading:after, .panel>.panel-heading:before {
	position: absolute;
	top: 11px;
	left: -16px;
	right: 100%;
	width: 0;
	height: 0;
	display: block;
	content: " ";
	border-color: transparent;
	border-style: solid solid outset;
	pointer-events: none;
}

.panel>.panel-heading:after {
	border-width: 7px;
	border-right-color: #f7f7f7;
	margin-top: 1px;
	margin-left: 2px;
}

.panel>.panel-heading:before {
	border-right-color: #ddd;
	border-width: 8px;
}

body {
	background: #fafafa;
}

.widget-area.blank {
	background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	-ms-box-shadow: none;
	-o-box-shadow: none;
	box-shadow: none;
}

body .no-padding {
	padding: 0;
}

.widget-area {
	background-color: #fff;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-ms-border-radius: 4px;
	-o-border-radius: 4px;
	border-radius: 4px;
	-webkit-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
	-ms-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
	-o-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
	box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
	float: left;
	margin-top: 30px;
	padding: 25px 30px;
	position: relative;
	width: 100%;
}

.status-upload {
	background: none repeat scroll 0 0 #f5f5f5;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-ms-border-radius: 4px;
	-o-border-radius: 4px;
	border-radius: 4px;
	float: left;
	width: 100%;
}

.status-upload form {
	float: left;
	width: 100%;
}

.status-upload form textarea {
	background: none repeat scroll 0 0 #fff;
	border: medium none;
	-webkit-border-radius: 4px 4px 0 0;
	-moz-border-radius: 4px 4px 0 0;
	-ms-border-radius: 4px 4px 0 0;
	-o-border-radius: 4px 4px 0 0;
	border-radius: 4px 4px 0 0;
	color: #777777;
	float: left;
	font-family: Lato;
	font-size: 14px;
	height: 142px;
	letter-spacing: 0.3px;
	padding: 20px;
	width: 100%;
	resize: vertical;
	outline: none;
	border: 1px solid #F2F2F2;
}

.status-upload ul {
	float: left;
	list-style: none outside none;
	margin: 0;
	padding: 0 0 0 15px;
	width: auto;
}

.status-upload ul>li {
	float: left;
}

.status-upload ul>li>a {
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-ms-border-radius: 4px;
	-o-border-radius: 4px;
	border-radius: 4px;
	color: #777777;
	float: left;
	font-size: 14px;
	height: 30px;
	line-height: 30px;
	margin: 10px 0 10px 10px;
	text-align: center;
	-webkit-transition: all 0.4s ease 0s;
	-moz-transition: all 0.4s ease 0s;
	-ms-transition: all 0.4s ease 0s;
	-o-transition: all 0.4s ease 0s;
	transition: all 0.4s ease 0s;
	width: 30px;
	cursor: pointer;
}

.status-upload ul>li>a:hover {
	background: none repeat scroll 0 0 #606060;
	color: #fff;
}

.status-upload form button {
	border: medium none;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-ms-border-radius: 4px;
	-o-border-radius: 4px;
	border-radius: 4px;
	color: #fff;
	float: right;
	font-family: Lato;
	font-size: 14px;
	letter-spacing: 0.3px;
	margin-right: 9px;
	margin-top: 9px;
	padding: 6px 15px;
}

.dropdown>a>span.green:before {
	border-left-color: #2dcb73;
}

.status-upload form button>i {
	margin-right: 7px;
}

.nanaa {
	
}
</style>
<%@include file="style.jsp"%>
</head>
<body>
	<!-- jQuery -->
	<script src="//code.jquery.com/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<%@include file="header.jsp"%>
	<%
		int ID = -1;
		Food food = null;
		if (request.getParameter("id") != null) {
			ID = Integer.parseInt(request.getParameter("id"));
			food = new FoodQuery().getFoodsByID(ID);
		}
		String comt = null;
		String pinContent = null;
		String like = null;
		like = request.getParameter("like");
		comt = request.getParameter("comment");
		pinContent = request.getParameter("pinContent");
		if (comt != null && comt.compareTo("") != 0) {
			if (session.getAttribute("user") == null) {
	%>
	<script type="text/javascript">
		alert("Bạn phải đăng nhập mới có thể bình luận !");
	</script>
	<%
		} else {
				JSONObject json = new JSONObject(session.getAttribute("user").toString());
				String idoftype = "";
				try {
					idoftype = json.getString("idoftype");
				} catch (Exception e) {
				}

				String email = json.getString("email");
				System.out.println("idoftype: " + idoftype + " email: " + email);
				System.out.println(idoftype == null);
				System.out.println(email == null);
				System.out.println(idoftype.compareTo("") == 0);
				System.out.println(email.compareTo("") == 0);
				User user = new UserQuery().getUserByEmailAndIdoftype(email, idoftype);
				Comment newCmt = new Comment();
				newCmt.setContent(comt);
				LocalDateTime now = LocalDateTime.now();
				String d = now.getDayOfMonth() + " tháng " + now.getMonthValue() + " lúc " + now.getHour() + ":"
						+ now.getSecond();
				newCmt.setDateTime(d);
				newCmt.setUserID("" + user.getId());
				food.getComments().add(newCmt);
				new SocialQuery().updateComments(food.getFoodCode(), food.getComments());
				food.getComments().get(food.getComments().size() - 1).setDateTime(" Vừa xong");
			}
		}
		if (pinContent != null && pinContent.compareTo("") != 0) {
			if (session.getAttribute("user") == null) {
	%>
	<script type="text/javascript">
		alert("Bạn phải đăng nhập mới có thể ghim nội dung này !");
	</script>
	<%
		} else {
				JSONObject json = new JSONObject(session.getAttribute("user").toString());
				String idoftype = json.getString("idoftype");
				String email = json.getString("email");
				User user = new UserQuery().getUserByEmailAndIdoftype(email, idoftype);
				Pin pin = new Pin();
				pin.setContent(pinContent);
				LocalDateTime now = LocalDateTime.now();
				String d = now.getDayOfMonth() + " tháng " + now.getMonthValue() + " lúc " + now.getHour() + ":"
						+ now.getSecond();
				pin.setDateTime(d);
				pin.setUserID("" + user.getId());
				food.getPins().add(pin);
				new SocialQuery().updatePins(food.getFoodCode(), food.getPins());
				food.getPins().get(food.getPins().size() - 1).setDateTime(" Vừa xong");
			}
		}
		if (like != null && like.compareTo("") != 0) {
			if (session.getAttribute("user") == null) {
	%>
	<script type="text/javascript">
		alert("Bạn phải đăng nhập mới có thể thích nội dung này !");
	</script>
	<%
		} else {
				JSONObject json = new JSONObject(session.getAttribute("user").toString());
				String idoftype = json.getString("idoftype");
				String email = json.getString("email");
				User user = new UserQuery().getUserByEmailAndIdoftype(email, idoftype);
				int id = user.getId();
				if (food.getLikes().contains(id)) {
					int idx = food.getLikes().indexOf(id);
					food.getLikes().remove(idx);
					new SocialQuery().updateLikes(food.getFoodCode(), food.getLikes());
				} else {
					food.getLikes().add(id);
					new SocialQuery().updateLikes(food.getFoodCode(), food.getLikes());
				}
			}
		}
		if (food != null) {
			ArrayList<Integer> userIDs = new ArrayList<Integer>();
			User userCr = new UserQuery().getUserByID(food.getUserId());
	%>
	<div class="container " style="background-color: white;">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans'
			rel='stylesheet' type='text/css'>
		<link
			href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
			rel="stylesheet">

		<div class="container"
			style="background: url('<%=food.getImageUrl()%>'); no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;">
			<div class="row panel">
				<div class="col-md-4 bg_blur"
					style=" background-image:url('<%=food.getImageUrl()%>');"></div>
				<div class="col-md-8  col-xs-12">
					<img src="http://lorempixel.com/output/people-q-c-100-100-1.jpg"
						class="img-thumbnail visible-xs picture_mob" />
					<div class="header">

						<h1 style="color: #1E90FF;">
							<%=food.getFoodName()%><br>
							____________________________________________ <br>
						</h1>
						<div class="col-sm-12">

							<a href="user/userDetail/userDetail.jsp?id=<%=userCr.getId()%>"><img
								class="img-circle" style="width: 50px;"
								src="<%=userCr.getUrlPicture()%>"> <b> <%=userCr.getName()%></b></a>
							<br> <br>
							<!-- /col-sm-1 -->
						</div>

						<h5 style="color: green;">
							<%
								out.print(food.getPrice());
							%>₫
						</h5>

						<h5 style="color: red">
							<%
								Address address = new AddressQuery().getAddressByID(food.getFoodAddress());
									out.print(address.getName());
							%>
						</h5>
						<span style="color: grey;">"<%=food.getDescription()%>"
						</span>
					</div>
				</div>
			</div>

			<div class="row nav">
				<div class="col-md-4"></div>
				<div class="col-md-8 col-xs-12" style="margin: 0px; padding: 0px;">
					<div class="col-md-4 col-xs-4 well">
						<i class="fa fa-weixin fa-lg"></i>
						<%
							int u = 0;
								if (food.getComments() != null)
									u = food.getComments().size();
								out.print(u);
						%>
					</div>
					<div class="col-md-4 col-xs-4 well">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
							class="glyphicon glyphicon-pushpin fa-lg"></i> <%
 	int s = 0;
 		if (food.getPins() != null)
 			s = food.getPins().size();
 		out.print(s);
 %></a>
						<ul id="login-dp" class="dropdown-menu" style="padding-top: 0px;">
							<li>
								<div class="row"
									style="background-color: #9ACD32; margin-top: 0px;">
									<div class="col-md-6"
										style="margin-top: 0px; margin-bottom: 50px; width: 500px;">
										<div class="widget-area no-padding blank"
											style="border: solid 2px white;">
											<div class="status-upload">
												<form>
													<h4 style="color: #9ACD32; text-align: center;">Ghim
														lên dòng thời gian của bạn</h4>
													<textarea name="id" placeholder="Bình luận ..."
														style="display: none;"><%=ID%></textarea>
													<textarea name="pinContent" placeholder="Nội dung ghim ..."
														style="border: solid #9ACD32 1 px; font-size: 20px;"></textarea>

													<button type="submit" class="btn btn-success "
														style="background-color: #9ACD32;">
														<i class="fa fa-share"></i> Ghim
													</button>

												</form>
											</div>
											<!-- Status Upload  -->
										</div>
										<!-- Widget Area -->
									</div>
								</div>
							</li>
						</ul>
						</li>

					</div>
					<div class="col-md-4 col-xs-4 well">
						<a
							href="foodDetail.jsp?id=<%=food.getFoodCode()%>&like=378483h3jefd87wh3f783bfhud7838hr"><img
							src="<%if (session.getAttribute("user") != null) {
					JSONObject json = new JSONObject(session.getAttribute("user").toString());
					String idoftype = json.getString("idoftype");
					String email = json.getString("email");
					User user = new UserQuery().getUserByEmailAndIdoftype(email, idoftype);
					int id = user.getId();
					if (food.getLikes().contains(id)) {
						out.print("icon/likeYes.png");
					} else {
						out.print("icon/likeNo.png");
					}
				} else {
					out.print("icon/likeNo.png");
				}%>"></img>
							<%
								int v = 0;
									if (food.getLikes() != null)
										v = food.getLikes().size();
									out.print(v);
							%></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container"
		style="background: url('http://blog.hdwallsource.com/wp-content/uploads/2015/01/food-wallpaper-45194-46390-hd-wallpapers.jpg'); background-repeat: no-repeat; background-attachment: fixed;">
		<nav
			style="width:650px;margin-right: 5px; margin-left:20px; margin-bottom: 10px;">
		<div class="col-sm-12">
			<h3>Viết bình luận của bạn</h3>
			<br>
		</div>
		<div class="row">
			<div class="col-md-6"
				style="margin-top: 0px; margin-bottom: 50px; width: 640px;">
				<div class="widget-area no-padding blank">
					<div class="status-upload">
						<form>
							<textarea name="id" placeholder="Bình luận ..."
								style="display: none;"><%=ID%></textarea>
							<textarea name="comment" placeholder="Bình luận ..."
								style="font-size: 20px;"></textarea>
							<ul>
								<li><a title="" data-toggle="tooltip"
									data-placement="bottom" data-original-title="Audio"><i
										class="fa fa-music"></i></a></li>
								<li><a title="" data-toggle="tooltip"
									data-placement="bottom" data-original-title="Video"><i
										class="fa fa-video-camera"></i></a></li>
								<li><a title="" data-toggle="tooltip"
									data-placement="bottom" data-original-title="Sound Record"><i
										class="fa fa-microphone"></i></a></li>
								<li><a title="" data-toggle="tooltip"
									data-placement="bottom" data-original-title="Picture"><i
										class="fa fa-picture-o"></i></a></li>
							</ul>
							<button type="submit" class="btn btn-success green">
								<i class="fa fa-share"></i> Viết
							</button>

						</form>
					</div>
					<!-- Status Upload  -->
				</div>
				<!-- Widget Area -->
			</div>

		</div>

		<%
			for (int i = 0; i < food.getComments().size(); i++) {
					Comment comment = food.getComments().get(i);
					User userCmt = new UserQuery().getUserByID(Integer.parseInt(comment.getUserID()));
		%>
		<div class="row">
			<div class="col-sm-1">
				<div class="thumbnail" style="width: 45px;">
					<img class="img-responsive user-photo"
						src="<%=userCmt.getUrlPicture()%>">
				</div>
				<!-- /thumbnail -->
			</div>
			<!-- /col-sm-1 -->

			<div class="col-sm-5">
				<div class="panel panel-default" style="width: 555px;">
					<div class="panel-heading">
						<a
							href="<%=request.getContextPath()%>/user/userDetail/userDetail.jsp?id=<%=userCmt.getId()%>"><strong
							style="color: blue;"><%=userCmt.getName()%></strong></a> <span
							class="text-muted"><%=comment.getDateTime()%></span>
					</div>
					<div class="panel-body"><%=comment.getContent()%></div>
					<!-- /panel-body -->
				</div>
				<!-- /panel panel-default -->
			</div>
			<!-- /col-sm-5 -->


		</div>
		<%
			}
		%> </nav>
		<nav id="pinPanel"
			style="margin-left:5px;  margin-right:5px; width: 470px;">
		<h3>Đã ghim</h3>
		<br>
		<%
			ArrayList<Pin> pins = food.getPins();
				for (int i = 0; i < pins.size(); i++) {
					User user = new UserQuery().getUserByID(Integer.parseInt(pins.get(i).getUserID()));
		%> <a class="pull-left"
			href="<%=request.getContextPath()%>/user/userDetail/userDetail.jsp?id=<%=user.getId()%>">
			<img class="avatar" style="margin: 5px; width: 70px; height: 70px;"
			src="<%=user.getUrlPicture()%>" alt="avatar">
		</a>
		<div class="comment-body" style="padding-right: 10px;">
			<div class="comment-heading">
				<h4 class="user">
					<%=user.getName()%>
				</h4>
				<h5 class="time" style="color: #A9A9A9"><%=pins.get(i).getDateTime()%></h5>
			</div>
			<p style="word-wrap: break-word; color: #9ACD32">
				#<%=pins.get(i).getContent()%>
			</p>
		</div>
		<%
			}
		%> </nav>
		<nav id="likePanel" style="margin-left:5px; width: 170px; ">
		<h3>Đã thích</h3>

		<%
			for (int i = 0; i < food.getLikes().size(); i++) {
					User user = new UserQuery().getUserByID(food.getLikes().get(i));
		%> <br>
		<div class="col-sm-12">
			<a
				href="<%=request.getContextPath()%>/user/userDetail/userDetail.jsp?id=<%=user.getId()%>"><img
				class="img-circle" style="width: 100px;"
				src="<%=user.getUrlPicture()%>"></a>
			<!-- /col-sm-1 -->
		</div>
		<%
			}
		%> </nav>

	</div>
	<!-- /container -->

	<%
		}
	%>
	<%
		if (comt != null && comt.compareTo("") != 0) {
			if (session.getAttribute("user") != null) {
	%>
	<script type="text/javascript">
		window.scrollTo(0, document.body.scrollHeight);
	</script>
	<%
		}
		}
		if (food.getLikes().size() == 0) {
	%>
	<script type="text/javascript">
		$("#likePanel").css('display', 'none');
	</script>
	<%
		}
		if (food.getPins().size() == 0) {
	%>
	<script type="text/javascript">
		$("#pinPanel").css('display', 'none');
	</script>
	<%
		}
	%>
	<%@include file="footer.jsp"%>
</body>
</html>