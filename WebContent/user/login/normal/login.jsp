<%@page import="com.food.user.UserQuery"%>
<%@page import="java.security.MessageDigest"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.food.user.User"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.food.db.DBConnect"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Đang tải ...</title>
</head>
<body>

	<%
		String email = request.getParameter("email");
		String passWord = request.getParameter("passWord");

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(passWord.getBytes());

		byte byteData[] = md.digest();
		//convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		passWord = sb.toString();

		if (email != null && passWord != null) {

			UserQuery userQuery = new UserQuery();
			User user = userQuery.getUserByEmailAndPassWord(email, passWord);

			if (user != null) {
				JSONObject jsonToCookie = new JSONObject(user);
				session.setAttribute("user", jsonToCookie);

				if (request.getParameterValues("checkbox") != null) {
					Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
					userCookie.setMaxAge(60 * 60 * 24 * 30);
					userCookie.setPath("/");
					response.addCookie(userCookie);
				}

				response.sendRedirect("../../../main.jsp");

			} else {
				response.sendRedirect("index.jsp");
			}

		} else {
			response.sendRedirect("index.jsp");
		}
	%>


</body>
</html>

