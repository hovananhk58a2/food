<%@page import="com.mysql.jdbc.DatabaseMetaData"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.food.db.DBConnect"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mysql.jdbc.jdbc2.optional.SuspendableXAConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="com.food.user.*"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../../../config.jsp"%>

<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	String code = request.getParameter("code");

	String httpsURL = "https://www.googleapis.com/oauth2/v4/token";

	StringBuilder q = new StringBuilder();
	q.append("client_id=" + URLEncoder.encode(google_client_id, "UTF-8"));
	q.append("&redirect_uri=" + URLEncoder.encode(google_redirect_uri, "UTF-8"));
	q.append("&client_secret=" + URLEncoder.encode(google_client_secret, "UTF-8"));
	q.append("&grant_type=" + URLEncoder.encode("authorization_code", "UTF-8"));
	q.append("&code=" + URLEncoder.encode(code, "UTF-8"));
	q.append("&oe=utf-8");

	String query = q.toString();
	URL myurl = new URL(httpsURL);
	HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
	con.setRequestMethod("POST");
	con.setRequestProperty("Content-length", String.valueOf(query.length()));
	con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
	con.setDoOutput(true);
	con.setDoInput(true);

	DataOutputStream output = new DataOutputStream(con.getOutputStream());

	output.writeBytes(query);
	output.close();

	DataInputStream input = new DataInputStream(con.getInputStream());
	String s = "";
	for (int c = input.read(); c != -1; c = input.read())
		s += String.valueOf((char) c);
	input.close();
	System.out.print(s.getBytes("UTF-8"));
	
	JSONObject json = new JSONObject(s);

	String accesstoken = json.getString("access_token");

	String $urlUserDetails = "https://www.googleapis.com/oauth2/v1/userinfo?oe=utf-8&alt=json&access_token="
			+ accesstoken;

	URL url = new URL($urlUserDetails);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("GET");
	connection.connect();

	DataInputStream reader = new DataInputStream(connection.getInputStream());

	String userString = "";
	for (int c = reader.read(); c != -1; c = reader.read())
		userString += String.valueOf((char) c);
	reader.close();

	UserQuery userQuery = new UserQuery();
	userQuery.checkTableExist("user");
	JSONObject userJson = new JSONObject(userString);

	String idOfType = "";
	String type = "google";
	User user = new User();

	try {

		idOfType = userJson.getString("id");
		user = userQuery.getUserByIdOfType(type, idOfType);
		// Check if user doesn't exists user in database
		if (user == null) {
			String name = "";
			String passWord = "";
			String first_name = "";
			String last_name = "";
			String email = "";
			String gender = "";
			String urlPicture = "";
			try {

				name = userJson.getString("name");
			} catch (Exception e) {
			}

			try {
				email = userJson.getString("email");
			} catch (Exception e) {
			}

			try {
				gender = userJson.getString("gender");

				if (gender.equals("male")) {
					gender = "Nam";
				} else if (gender.equals("female")) {
					gender = "Nữ";
				}

			} catch (Exception e) {
			}

			try {
				urlPicture = userJson.getString("picture");

			} catch (Exception e) {
			}

			user = new User(0, name, passWord, email, gender, urlPicture, type, idOfType);
			// Save user to database
			userQuery.saveUserToDatabase(user);
		}

	} catch (Exception e) {

	}

	JSONObject jsonToCookie = new JSONObject(user);
	session.setAttribute("user", jsonToCookie);
	Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
	userCookie.setMaxAge(60 * 60 * 24 * 30);
	userCookie.setPath("/");
	response.addCookie(userCookie);
	response.sendRedirect("../../../main.jsp");
%>