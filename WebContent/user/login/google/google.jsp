<%@page import="java.security.SecureRandom"%>
<%@page import="java.math.BigInteger"%>
<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../../../config.jsp"%>
<%
	String urlAuthorize = "https://accounts.google.com/o/oauth2/v2/auth?client_id=" + google_client_id
			+ "&response_type=code&scope=" + google_scope + "&redirect_uri=" + google_redirect_uri;
	response.sendRedirect(urlAuthorize);
%>
