<%@page import="com.food.user.UserQuery"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DatabaseMetaData"%>
<%@page import="java.security.MessageDigest"%>
<%@page import="com.food.db.DBConnect"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.food.user.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html">
<title>Loading ...</title>
</head>
<body>
	<%
		String email = request.getParameter("emailResgiter");
		UserQuery userQuery = new UserQuery();
		userQuery.checkTableExist("user");
		
		if (userQuery.checkUserByEmail(email) == true) {
			response.sendRedirect("../login/normal/index.jsp?registered=" + email);
			return;
		}

		String name = request.getParameter("name");
		String urlPicture = "https://scontent.fhan3-1.fna.fbcdn.net/v/t1.0-9/14590372_603767893129596_7463257834055002695_n.jpg?oh=a1c3eecd9b3fc3bcb1c8ab66e505892f&oe=591669DF";
		String gender = request.getParameter("gender");
		String type = "normal";
		String idOfType = "";
		String passWord = request.getParameter("passWord");

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(passWord.getBytes());
		byte byteData[] = md.digest();
		//convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		passWord = sb.toString();
		User user = new User(0, name, passWord, email, gender, urlPicture, type, idOfType);
		userQuery.saveUserToDatabase(user);

		// Cooki session
		JSONObject jsonToCookie = new JSONObject(user);
		session.setAttribute("user", jsonToCookie);
		Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
		userCookie.setMaxAge(60 * 60 * 24 * 30);
		userCookie.setPath("/");
		response.addCookie(userCookie);
		response.sendRedirect("../../main.jsp");
	%>
</body>
</html>