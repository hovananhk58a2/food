<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ăn gì ?</title>
<%@include file="../../style.jsp"%>
<style type="text/css">
div.register {
	width: 100%;
	overflow-y: hidden;
	padding: 5px;
	padding-right: 200px;
	padding-top: 100px;
	color: white;
	background-size: cover;
	background:
		url("../../icon/food-wallpaper.jpg");
}
</style>
</head>
<body>
	<jsp:include page="../../header.jsp"></jsp:include>

	<div class="register">
		<form 
			action="<%=request.getContextPath()%>/user/register/createUser.jsp"
			class="form-horizontal">
			
			<div class="form-group" style="padding: 15px" >
				<label class="col-sm-3 control-label">Tên đăng nhập *</label>
				<div class="col-sm-9">
					<input autocomplete="off" type="text" name="name" class="form-control" required></input>
				</div>
			</div>
			
			<div class="form-group" style="padding: 15px">
				<label class="col-sm-3 control-label">Email *</label>
				<div class="col-sm-9">
					<input autocomplete="off" type="email" id="email" name="emailResgiter"
						class="form-control" required ">
				</div>
			</div>
			
			<div class="form-group" style="padding: 15px">
				<label class="col-sm-3 control-label">Mật khẩu *</label>
				<div class="col-sm-9">
					<input autocomplete="off" required type="password" id="password" name="passWord"
						class="form-control">
				</div>
			</div>

			<div class="form-group" style="padding: 15px">
				<label class="col-sm-3 control-label">Nhập lại mật khẩu</label>
				<div class="col-sm-9">

					<input autocomplete="new-password" required type="password" id="confirm_password"
						name="confirm_password" class="form-control">
				</div>
			</div>

			<div class="form-group" style="padding-top: 15px">
				<label class="control-label col-sm-3">Giới tính</label>
				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-4">
							<label class="radio-inline"> <input type="radio"
								name="gender" id="femaleRadio" value="Nam">Nam
							</label>
						</div>
						<div class="col-sm-4">
							<label class="radio-inline"> <input type="radio"
								name="gender" id="maleRadio" value="Nữ">Nữ
							</label>
						</div>
						<div class="col-sm-4">
							<label class="radio-inline"> <input type="radio"
								name="gender" id="uncknownRadio" value="Khác">Khác
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<div class="checkbox">
						<label> <input type="checkbox" required>Chấp nhận
							<a href="<%=request.getContextPath()%>/user/register/Rules.html">Điều khoản và Bảo mật</a>
						</label>
					</div>
				</div>
			</div>
			<!-- /.form-group -->
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="submit" class="btn btn-primary btn-block">Đăng
						kí</button>
				</div>
			</div>
		</form>

	</div>

	<script src="//code.jquery.com/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->



	<script>
		var password = document.getElementById("password"), confirm_password = document
				.getElementById("confirm_password");

		function validatePassword() {
			if (password.value != confirm_password.value) {
				confirm_password.setCustomValidity("Mật khẩu không giống");
			} else {
				confirm_password.setCustomValidity('');
			}
		}

		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;
	</script>
</body>
</html>