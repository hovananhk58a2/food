<%@page import="java.net.URLEncoder"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.food.user.User"%>
<%@page import="com.mysql.jdbc.DatabaseMetaData"%>
<%@page import="com.food.db.DBConnect"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Loading ...</title>
</head>
<body>
	<%
		request.setCharacterEncoding("UTF-8");

		if (session.getAttribute("user") != null) {
			JSONObject json = new JSONObject(session.getAttribute("user").toString());
			String name = json.getString("name");
			try {
				String role = json.getString("type");
				if (role.equals("admin")) {
					DBConnect connect = new DBConnect();
					String table = "food";
					String id = request.getParameter("foodId");

					try {
						String command = " DELETE FROM " + table + " WHERE foodcode=" + id;
						connect.executeUpdate(command);
						response.sendRedirect("userDetail.jsp?id=" + json.getInt("id"));
					} catch (Exception e) {
						out.print(e + "error");
					}
					connect.closeCon();
				}
			} catch (Exception e) {

			}

		}
	%>
</body>
</html>