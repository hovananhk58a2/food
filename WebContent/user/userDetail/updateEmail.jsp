<%@page import="java.net.URLEncoder"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.food.user.User"%>
<%@page import="com.mysql.jdbc.DatabaseMetaData"%>
<%@page import="com.food.db.DBConnect"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Loading ...</title>
</head>
<body>
	<%
		request.setCharacterEncoding("UTF-8");
		DBConnect connect = new DBConnect();
		String table = "user";
		String newEmail = request.getParameter("newEmail");

		JSONObject json = new JSONObject(session.getAttribute("user").toString());
		String oldName = json.getString("name");
		String oldPassWord = json.getString("passWord");
		String oldGender = json.getString("gender");
		String oldUrlPicture = json.getString("urlPicture");
		String oldEmail = json.getString("email");
		String oldType = json.getString("type");
		String oldIdoftype = json.getString("idoftype");
		String id = "";

		String sql = "SELECT * FROM " + table + " WHERE name='" + oldName + "' AND urlPicture ='" + oldUrlPicture
				+ "' AND gender ='" + oldGender + "' AND email ='" + oldEmail + "'";
		ResultSet rs = connect.getRs(sql);
		try {
			while (rs.next()) {
				id = rs.getString("id");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			String command = " UPDATE " + table + " SET email='" + newEmail+ "' WHERE id=" + id;
			out.print(command);
			connect.executeUpdate(command);
		} catch (Exception e) {
			out.print(e + "error");
		}

		
		User user = new User(json.getInt("id"), oldName,oldPassWord, newEmail, oldGender, oldUrlPicture, oldType, oldIdoftype);
		JSONObject jsonToCookie = new JSONObject(user);

		session.setAttribute("user", jsonToCookie);
		Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
		userCookie.setMaxAge(60 * 60 * 24 * 30);
		userCookie.setPath("/");
		response.addCookie(userCookie);
		connect.closeCon();
		response.sendRedirect("userDetail.jsp?id=" + json.getInt("id"));
	%>
</body>
</html>