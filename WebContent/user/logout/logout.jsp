<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	session.setAttribute("user", null);

	Cookie killMyCookie = new Cookie("user", null);
	killMyCookie.setMaxAge(0);
	killMyCookie.setPath("/");
	response.addCookie(killMyCookie);

	response.sendRedirect("../../main.jsp");
%>
