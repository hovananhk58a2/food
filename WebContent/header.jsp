<%@page import="java.security.MessageDigest"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@page
	import="com.oracle.xmlns.internal.webservices.jaxws_databinding.ExistingAnnotationsType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<html>
<head>
<%@include file="style.jsp"%>
</head>
<body>
	<header id="header" class="navbar navbar-default" role="navigation"
		style="background-color: white; width: 100%; border: 1px solid #e0e0e0; z-index: 1; position: fixed;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="<%=request.getContextPath()%>/main.jsp"
					style="background-color: #ed3554; color: white;"><b><img
						src="<%=request.getContextPath()%>/icon/husFood.png" height="21"
						width="33"> Ăn gì ?</b></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/diadiem.jsp">Địa
							điểm</a></li>
				</ul>
				<form class="navbar-form navbar-left" role="search"
					action="<%=request.getContextPath()%>/searchResult.jsp">
					<div class="form-group">
						<b>Tìm kiếm </b> <input name="search" id="nhaptimkiem" type="text"
							class="form-control" style="width: 400px;"
							placeholder="món ăn, địa điểm, mọi thứ...">
					</div>
					<button id="timkiembt" type="submit"
						class="btn btn-default form-control">
						<img src="<%=request.getContextPath()%>/icon/search.png"
							height="22" width="25">
					</button>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<%
						Cookie cookie = null;
						Cookie[] cookies = null;
						String urlPicture = "";

						cookies = request.getCookies();
						if (cookies != null) {
							for (int i = 0; i < cookies.length; i++) {
								cookie = cookies[i];
								if (cookie.getName().equals("user")) {
									JSONObject userJson = new JSONObject(URLDecoder.decode(cookie.getValue(), "UTF-8"));
									session.setAttribute("user", userJson);
								}
							}
						}

						if (session.getAttribute("user") != null) {
							JSONObject json = new JSONObject(session.getAttribute("user").toString());
							String name = json.getString("name");
							String type = json.getString("type");
							urlPicture = json.getString("urlPicture");

							if (!type.equals("admin")) {
					%><li id="dangban"><a
						href="<%=request.getContextPath()%>/sell.jsp">Đăng bán</a></li>
					<%
						}
					%>

					<li id="user"><a
						href="<%=request.getContextPath()%>/user/userDetail/userDetail.jsp?id=<%out.print(json.getInt("id"));%>">
							<img id="foodimage" class="img-circle" src="<%=urlPicture%>"
							height="22" width="25"> <%
 	String nameDisplay = name;
 		if (name.length() > 11)
 			nameDisplay = name.substring(0, 11) + "...";
 		out.println(nameDisplay);
 %>
					</a></li>

					<li id="user"><a
						href="<%=request.getContextPath()%>/user/logout/logout.jsp">Đăng
							xuất</a></li>
					<%
						} else {
					%>
					<li id="dangnhap" class="dropdown"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown">Đăng nhập<b
							class="caret"></b></a>
						<ul id="login-dp" class="dropdown-menu">
							<li>
								<div class="row">
									<div class="col-md-12">
										Đăng nhập bằng
										<div class="social-buttons">
											<a
												href="<%=request.getContextPath()%>/user/login/facebook/facebook.jsp"
												class="btn btn-fb">Facebook</a> <a
												href="<%=request.getContextPath()%>/user/login/google/google.jsp"
												class="btn btn-gg">Google+</a>
										</div>
										Tài khoản Husfood ?
										<form class="form" role="form" method="post"
											action="<%=request.getContextPath()%>/user/login/normal/login.jsp"
											accept-charset="UTF-8" id="login-nav">
											<div class="form-group">
												<label class="sr-only" for="exampleInputEmail2">Email</label>
												<input class="form-control" name="email" type="email"
													id="exampleInputEmail2" placeholder="Email" required>
											</div>
											<div class="form-group">
												<label class="sr-only" for="exampleInputPassword2">Mật
													khẩu</label> <input type="password" class="form-control"
													name="passWord" id="exampleInputPassword2"
													placeholder="Mật khẩu" required>
												<div class="help-block text-right">
													<a href="">Quên mật khẩu ?</a>
												</div>
											</div>
											<div class="checkbox">
												<label> <input name="checkbox" type="checkbox">
													Ghi nhớ tôi
												</label>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary btn-block">Đăng
													nhập</button>
											</div>

										</form>
									</div>
									<div class="bottom text-center">
										Chưa có tài khoản ? <a
											href="<%=request.getContextPath()%>/user/register/register.jsp"><b>Đăng
												ký</b></a>
									</div>
								</div>
							</li>
						</ul></li>

					<%
						}
					%>

				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<script type="text/javascript">
			$("#register-form")
					.submit(
							function() {
								var isFormValid = true;

								$(".required input").each(function() {
									if ($.trim($(this).val()).length == 0) {
										$(this).addClass("highlight");
										isFormValid = false;
									} else {
										$(this).removeClass("highlight");
									}
								});

								if (!isFormValid)
									alert("Please fill in all the required fields (indicated by *)");

								return isFormValid;
							});
		</script>
		<script type="text/javascript"
			src="http://code.jquery.com/jquery-latest.js"></script>
	</header>

</body>
</html>