<%@page import="com.food.user.User"%>
<%@page import="com.food.address.AddressQuery"%>
<%@page import="com.food.user.UserQuery"%>
<%@page import="com.food.table.Food"%>
<%@page import="com.food.search.ExcuteSearch"%>
<%@page import="com.food.search.SearchResult"%>
<%@page import="com.food.table.FoodClass"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.food.query.FoodQuery"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="">
<head>
<title>Ăn gì ?</title>
<%@include file="style.jsp"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
@import "http://fonts.googleapis.com/css?family=Roboto:300,400,500,700";

.mb20 {
	margin-bottom: 20px;
}

hgroup {
	padding-left: 15px;
	border-bottom: 1px solid #ccc;
}

hgroup h1 {
	font: 500 normal 1.625em "Roboto", Arial, Verdana, sans-serif;
	color: #2a3644;
	margin-top: 0;
	line-height: 1.15;
}

hgroup h2.lead {
	font: normal normal 1.125em "Roboto", Arial, Verdana, sans-serif;
	color: #2a3644;
	margin: 0;
	padding-bottom: 10px;
}

.search-result .thumbnail {
	border-radius: 0 !important;
}

.search-result:first-child {
	margin-top: 0 !important;
}

.search-result {
	margin-top: 20px;
	padding-top: 10px;
}

.search-result .col-md-2 {
	border-right: 1px dotted #ccc;
	min-height: 140px;
}

.search-result ul {
	padding-left: 0 !important;
	list-style: none;
}

.search-result ul li {
	font: 400 normal .85em "Roboto", Arial, Verdana, sans-serif;
	line-height: 30px;
}

.search-result ul li i {
	padding-right: 5px;
}

.search-result .col-md-7 {
	position: relative;
}

.search-result h3 {
	font: 500 normal 1.375em "Roboto", Arial, Verdana, sans-serif;
	margin-top: 0 !important;
	margin-bottom: 10px !important;
}

.search-result h3>a, .search-result i {
	color: #248dc1 !important;
}

.search-result p {
	font: normal normal 1.125em "Roboto", Arial, Verdana, sans-serif;
}

.search-result span.plus {
	position: absolute;
	right: 0;
	top: 126px;
}

.search-result span.plus a {
	background-color: #248dc1;
	padding: 5px 5px 3px 5px;
}

.search-result span.plus a:hover {
	background-color: #414141;
}

.search-result span.plus a i {
	color: #fff !important;
}

.search-result span.border {
	display: block;
	width: 97%;
	margin: 0 15px;
	border-bottom: 1px dotted #ccc;
}

.nav-tabs {
	border-bottom: 2px solid #DDD;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
	{
	border-width: 0;
}

.nav-tabs>li>a {
	border: none;
	color: #666;
}

.nav-tabs>li.active>a, .nav-tabs>li>a:hover {
	border: none;
	color: #4285F4 !important;
	background: transparent;
}

.nav-tabs>li>a::after {
	content: "";
	background: #4285F4;
	height: 2px;
	position: absolute;
	width: 100%;
	left: 0px;
	bottom: -1px;
	transition: all 250ms ease 0s;
	transform: scale(0);
}

.nav-tabs>li.active>a::after, .nav-tabs>li:hover>a::after {
	transform: scale(1);
}

.tab-nav>li>a::after {
	background: #21527d none repeat scroll 0% 0%;
	color: #fff;
}

.tab-pane {
	padding: 15px 0;
}

.tab-content {
	padding: 20px
}

.card {
	background: #FFF none repeat scroll 0% 0%;
	box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
	margin-bottom: 30px;
}
</style>
<title>Tìm kiếm?</title>
</head>

<body>

	<jsp:include page="header.jsp"></jsp:include>
	<%
		String searCh = null;
		searCh = request.getParameter("search");
		String temp = searCh;
		searCh = searCh.replaceAll("	", "");
		searCh = searCh.replaceAll(" ", "");
		SearchResult result = null;
		if (searCh != null && searCh.compareTo("") != 0) {
			result = new ExcuteSearch().search(searCh);
		}else{
			result = new SearchResult();
		}
	%>
	<div class="container">
		<div class="row">
			<div class="col-md-6" style="width: 100%;">
				<!-- Nav tabs -->
				<div class="card">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home"
							aria-controls="home" role="tab" data-toggle="tab">Món ăn</a></li>
						<li role="presentation"><a href="#profile"
							aria-controls="profile" role="tab" data-toggle="tab">Người
								dùng</a></li>
						<!-- <li role="presentation"><a href="#messages"
							aria-controls="messages" role="tab" data-toggle="tab">A</a></li>
						<li role="presentation"><a href="#settings"
							aria-controls="settings" role="tab" data-toggle="tab">B</a></li>
							-->
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<hgroup class="mb20">
								<h1>Kết quả tìm kiếm</h1>
								<h2 class="lead">
									<strong class="text-danger"><%=result.getFoods().size()%></strong>
									kết quả cho từ khóa <strong class="text-danger"><%=temp%></strong>
								</h2>
							</hgroup>

							<section class="col-xs-12 col-sm-6 col-md-12">
								<%
									for (int i = 0; i < result.getFoods().size(); i++) {
										Food food = result.getFoods().get(i);
								%>
								<article class="search-result row">
									<div class="col-xs-12 col-sm-12 col-md-3">
										<a
											href="<%=request.getContextPath()%>/foodDetail.jsp?id=<%=food.getFoodCode()%>"
											title="Lorem ipsum" class="thumbnail"><img
											src="<%=food.getImageUrl()%>" alt="Lorem ipsum" /></a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-2">
										<ul class="meta-search">
											<li><img
												src="<%=request.getContextPath()%>/icon/dollar.png"
												style="width: 16px; height: 16px;"></img> <span
												style="color: green;"><%=food.getPrice()%>₫</span></li>
											<li><img
												src="<%=request.getContextPath()%>/icon/map-with-a-pin.png"
												style="width: 16px; height: 16px;"></img> <span
												style="color: red;"><%=new AddressQuery().getAddressByID("" + food.getFoodAddress()).getName()%></span></li>
											<li><img
												src="<%=request.getContextPath()%>/icon/user-silhouette.png"
												style="width: 16px; height: 16px;"></img><span
												style="color: blue;"><%=new UserQuery().getUserByID(food.getUserId()).getName()%></span></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-7 excerpet">
										<h3>
											<a
												href="<%=request.getContextPath()%>/foodDetail.jsp?id=<%=food.getFoodCode()%>"
												title=""><%=food.getFoodName()%></a>
										</h3>
										<p>
											"<%=food.getDescription()%>"
										</p>
										<span class="plus"><a href="#" title="Lorem ipsum"><i
												class="glyphicon glyphicon-plus"></i></a></span>
									</div>
									<span class="clearfix borda"></span>
								</article>
								<%
									}
								%>
							</section>
						</div>
						<div role="tabpanel" class="tab-pane" id="profile">
							<hgroup class="mb20">
								<h1>Kết quả tìm kiếm</h1>
								<h2 class="lead">
									<strong class="text-danger"><%=result.getUsers().size()%></strong>
									kết quả cho từ khóa <strong class="text-danger"><%=temp%></strong>
								</h2>
							</hgroup>

							<section class="col-xs-12 col-sm-6 col-md-12">
								<%
									for (int i = 0; i < result.getUsers().size(); i++) {
										User user = result.getUsers().get(i);
								%>
								<article class="search-result row">
									<div class="col-xs-12 col-sm-12 col-md-3">
										<a
											href="<%=request.getContextPath()%>/user/userDetail/userDetail.jsp?id=<%=user.getId()%>"
											title="Lorem ipsum" class="thumbnail"><img
											src="<%=user.getUrlPicture()%>" alt="Lorem ipsum" /></a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-2">
										<ul class="meta-search">
											<li><img
												src="<%=request.getContextPath()%>/icon/edit.png"
												style="width: 16px; height: 16px;"></img> <span
												style="color: green;"><%=new UserQuery().getPublicNums(user.getId())%> bài đăng</span></li>
											<li><img
												src="<%=request.getContextPath()%>/icon/envelope.png"
												style="width: 16px; height: 16px;"></img> <span
												style="color: red;"><%=user.getEmail()%></span></li>
											<li><img
												src="<%=request.getContextPath()%>/icon/users-group.png"
												style="width: 16px; height: 16px;"></img><span
												style="color: blue;"><%=user.getType()%></span></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-7 excerpet">
										<h3>
											<a
												href="<%=request.getContextPath()%>/user/userDetail/userDetail.jsp?id=<%=user.getId()%>"
												title=""><%=user.getName()%></a>
										</h3>
										<p>
											"Thành viên của HUS Food :)"
										</p>
										<span class="plus"><a href="#" title="Lorem ipsum"><i
												class="glyphicon glyphicon-plus"></i></a></span>
									</div>
									<span class="clearfix borda"></span>
								</article>
								<%
									}
								%>
							</section>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container"></div>

	<footer>
		<jsp:include page="footer.jsp"></jsp:include>
	</footer>

	<!-- jQuery -->
	<script src="//code.jquery.com/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

</body>

</html>