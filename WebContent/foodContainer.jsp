<%@page import="com.food.table.Food"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.food.query.FoodQuery"%>
<%@page import="com.food.db.DBConnect"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	int classCode = 1;
	String className = "Tất cả";
	if (request.getParameter("fclass") != null) {
		classCode = Integer.parseInt(request.getParameter("fclass"));
	}
	if (request.getParameter("name") != null) {
		className = request.getParameter("name");
	}
%>
<script>
	$("#infoContain").html("");
</script>
<div>
	<h3 id="love" style="text-align: center;"><%=className%></h3>
</div>

<%
	FoodQuery foodQuery = new FoodQuery();
	ArrayList<Food> list = foodQuery.getFoodsByCategory(classCode);
	for (int r = 0; r < list.size(); r++) {
		Food foodr = list.get(r);
		if (foodr.getChecked() != 0) {
%>
<div class="img"
	style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px; margin-bottom: 40px;">
	<div class="hovereffect">
		<a href="foodDetail.jsp?id=<%=foodr.getFoodCode()%>"><img
			id="foodimage" src=<%=foodr.getImageUrl()%> alt="Tên món"></a>
		<div class="overlay">

			<p class="icon-links">
				<a href="foodDetail.jsp?id=<%=foodr.getFoodCode()%>"><img
					alt="Ghim" src="icon/push-pin.png"
					style="width: 24px; height: 24px;"> </a> <a
					href="foodDetail.jsp?id=<%=foodr.getFoodCode()%>"> <img
					alt="Yêu thích" src="icon/like.png"
					style="width: 24px; height: 24px;"></a> <a
					href="foodDetail.jsp?id=<%=foodr.getFoodCode()%>"><img
					alt="Chat" src="icon/chat.png"
					style="width: 24px; height: 24px; margin-right: 40px;"> </a>
			</p>
		</div>
	</div>
	<div class="desc"><%=foodr.getFoodName()%></div>
</div>

<%
	}
	}
%>
