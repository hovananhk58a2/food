
<%@page import="com.food.table.FoodClass"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.food.query.FoodQuery"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet"
	href="css/footer-distributed-with-address-and-phones.css">

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link href="http://fonts.googleapis.com/css?family=Cookie"
	rel="stylesheet" type="text/css">

<title>Đăng Bán</title>
<%@include file="style.jsp"%>
<style>
h1 {
	font color: yellowgreen;
	font-style: italic;
	text-align: center;
}

#i_file {
	background: #98FB98;
}

.dangban {
	background: FF3333; /*màu nền của nút*/
	border: 3px #00ccff outset;
	padding: 0 2px;
	text-decoration: none;
	font: bold 20px Verdana; /*kích thước của text*/
	color: #ff0000; /*màu text*/
}

.dangban:hover {
	border-style: inset;
	background: #FF00FF; /*màu nền của nút khi rê chuột*/
	padding: 2px 3px 0 5px;
	color: #000099; /*màu text khi rê chuột*/
}

.dangban:visited {
	background: #00ffff; /*màu nền của nút sau khi click chuột*/
	color: #ff6600; /*màu text khi đã click chuột*/
}

.dangban:active {
	color: white;
}

.container {
	width: 100%;
	height: 100%;
	margin: 0px;
	background: url(icon/hoadao.jpg);
}
</style>
<meta http-equiv="Content-Type" content="text/html" ; charset=UTF-8>

</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<!-- jQuery -->
	<script src="//code.jquery.com/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->


	<br>
	</br>
	<div class="container">


		<form action="sell-main.jsp" method="post" name="f1"
			style="width: 100%; height: 100%; margin: 0 auto; margin-top: 50px; border-radius: 5px; padding: 5px;">
			<table>
				<tr>
					<td><h3>Tên Sản Phẩm</h3></td>
					<td><input type="text" name="foodname" required /></td>
				</tr>
				<tr>
					<td><h3>Địa chỉ</h3></td>
					<td><input type="text" name="foodaddress" required /></td>
				</tr>
				<tr>
					<td><h3>Ảnh</h3></td>
					<td><input type="file" id="i_file" name="imageurl" required></td>


					<td><input type="text" id="foodAbUrl" name="abcUrl"
						style="display: none;"></td>
				</tr>
				<tr>
					<img class="imgfade"
						src="icon/picture.png"
						style="algin: right; max-width: 150px; max-height: 1500px;"></img>
				</tr>
				<tr>
					<td><h3>Chủ đề món ăn</h3></td>
					<td bordercolor="red"><input type="checkbox" name="foodclass"
						value="2"> Ăn với người yêu <input type="checkbox"
						name="foodclass" value="3"> Góc cà phê <input
						type="checkbox" name="foodclass" value="4">Ăn với gia đình
						<br> <input type="checkbox" name="foodclass" value="5">
						Ăn với nhóm <input type="checkbox" name="foodclass" value="6">Tiệm
						bánh <input type="checkbox" name="foodclass" value="7">Hải
						sản<br> <input type="checkbox" name="foodclass" value="8">Nướng
						& Lẩu <input type="checkbox" name="foodclass" value="9">Nhật
						Bản <input type="checkbox" name="foodclass" value="10">
						Pizza <br> <input type="checkbox" name="foodclass" value="11">
						Ăn vặt & Vỉa hè <input type="checkbox" name="foodclass" value="12">
						Ăn một mình <input type="checkbox" name="foodclass" value="13">
						Hàn Quốc<br></td>
				</tr>
				<tr>
					<td><h3>Mô tả sản phẩm</h3></td>
					<td><input type="text" name="description" required></td>
				</tr>
				<tr>
					<td><h3>Giá</h3></td>
					<td><input type="number" name="price" required></td>

				</tr>
				<tr>
					<td></td>
					<td><input type="submit" class="dangban" value="Submit"
						name="btnOK" /></td>
				</tr>
			</table>
		</form>

	</div>
	<script type="text/javascript">
		$('#i_file')
				.change(
						function(event) {
							var tmppath = URL
									.createObjectURL(event.target.files[0]);
							$(".imgfade").fadeIn("fast").attr('src',
									URL.createObjectURL(event.target.files[0]));

							$("#disp_tmp_paoth")
									.html(
											"Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["
													+ tmppath + "]</strong>");

							document.getElementById("foodAbUrl").value = tmppath;
						});
	</script>

	<%@include file="footer.jsp"%>
</body>
</html>