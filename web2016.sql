-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2017 at 06:23 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web2016`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isPopular` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `name`, `isPopular`) VALUES
(1, '322 Nguyễn Trãi, Hà Nội', 1),
(2, '33 Nguyễn Thị Định, Quận Cầu Giấy, Hà Nội', 1),
(3, '79 Trần Hưng Đạo, Quận Hoàn Kiếm, Hà Nội', 0),
(4, '32C Cao Bá Quát, Quận Ba Đình, Hà Nội', 0),
(5, '20 Nguyễn Văn Huyên Kéo Dài, Quận Cầu Giấy', 1),
(6, '110 Đào Tấn, Quận Ba Đình, Hà Nội', 0),
(7, '106 Hào Nam Kéo Dài, Quận Đống Đa, Hà Nội', 1),
(8, '1A Tràng Tiền, P. Phan Chu Trinh, Quận Hoàn Kiếm', 1),
(9, '5 Ngõ 4 Ao Sen, Quận Hà Đông, Hà Nội', 0),
(10, '53 Ô Chợ Dừa, Quận Đống Đa, Hà Nội', 0),
(11, '28 Chùa Láng, Quận Đống Đa, Hà Nội', 0),
(12, '68A Nguyễn Hữu Huân, Quận Hoàn Kiếm', 0),
(13, '65C Trần Quốc Toản, Quận Hoàn Kiếm, Hà Nội', 0),
(14, '44 Tuệ Tĩnh, Quận Hai Bà Trưng, Hà Nội', 0),
(15, '19 Thái Phiên, Quận Hai Bà Trưng, Hà Nội', 0),
(16, '35 - 37 Thái Thịnh, Quận Đống Đa, Hà Nội', 0),
(17, '46A Nguyễn Văn Ngọc, Quận Ba Đình, Hà Nội', 0),
(18, '3 Nguyễn Siêu, Quận Hoàn Kiếm, Hà Nội', 0),
(19, 'Tầng 1, Mipec Tower, 229 Tây Sơn, Quận Đống Đa', 0),
(20, '57 Hai Bà Trưng, Quận Hoàn Kiếm, Hà Nội', 0),
(21, 'Tầng 1, TTTM D2 Giảng Võ, Quận Đống Đa', 0),
(22, 'Ngõ 170 Ngọc Hà, Quận Ba Đình, Hà Nội', 0),
(23, '60 Lý Thường Kiệt, Quận Hoàn Kiếm, Hà Nội', 0),
(24, '25 - 27 Lê Thái Tổ, Quận Hoàn Kiếm, Hà Nội', 0),
(25, '11A Nguyễn Khuyến, Quận Đống Đa, Hà Nội', 0),
(26, '24 Tông Đản, Quận Hoàn Kiếm, Hà Nội', 0),
(27, '151 Ô Chợ Dừa, Quận Đống Đa, Hà Nội', 0),
(28, '49C Trần Quốc Toản, Quận Hoàn Kiếm, Hà Nội', 0),
(29, '87 Triệu Việt Vương, Quận Hai Bà Trưng', 0),
(30, '18 Ngõ 19 Đông Tác, Quận Đống Đa', 0),
(31, '31i Láng Hạ, Quận Ba Đình, Hà Nội', 0),
(32, '25 Láng Hạ, Quận Ba Đình, Hà Nội', 0),
(33, '26 Trung Hòa, Quận Cầu Giấy, Hà Nội', 0),
(34, '85 Thái Hà, Quận Đống Đa, Hà Nội', 0),
(35, '166 Ô Chợ Dừa, Quận Đống Đa, Hà Nội', 0),
(36, '24 Tông Đản, Quận Hoàn Kiếm, Hà Nội', 0),
(39, '24 Tông Đản, Quận Hoàn Kiếm, Hà Nội', 0),
(37, '151 Ô Chợ Dừa, Quận Đống Đa, Hà Nội', 0),
(41, '24 Tông Đản, Quận Hoàn Kiếm, Hà Nội', 0),
(50, '24 Tông Đản, Quận Hoàn Kiếm, Hà Nội', 0),
(51, '151 Ô Chợ Dừa, Quận Đống Đa, Hà Nội', 0);

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `foodcode` int(11) NOT NULL,
  `foodname` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `foodaddress` int(11) DEFAULT NULL,
  `foodclass` json NOT NULL,
  `price` int(11) DEFAULT NULL,
  `imageurl` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `likes` json DEFAULT NULL,
  `pins` json DEFAULT NULL,
  `comments` json DEFAULT NULL,
  `description` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checked` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`foodcode`, `foodname`, `foodaddress`, `foodclass`, `price`, `imageurl`, `userid`, `likes`, `pins`, `comments`, `description`, `checked`) VALUES
(1, 'Cháo sườn', 1, '[2, 3, 9, 10, 11, 4]', 20000, 'http://lamthenao.me/wp-content/uploads/2016/05/chao-tom-cho-be-an-dam.jpg', 19, NULL, '[{"time": " Mon Jan 09 02:45:07 ICT 2017", "userID": "15", "content": "chủ nhật tuần này đi ăn món này mới được !"}, {"time": " Mon Jan 09 02:54:27 ICT 2017", "userID": "13", "content": "Nice, mọi người nên ăn thử !"}, {"time": "13 tháng 1 lúc 16:22", "userID": "14", "content": "Thật tuyệt"}, {"time": "13 tháng 1 lúc 16:55", "userID": "14", "content": "Không thể cưỡng lại với món ăn ngon như thế này các bạn của tôi ơi !"}, {"time": "13 tháng 1 lúc 17:26", "userID": "14", "content": "#Không thể cưỡng lại với món ăn ngon như thế này các bạn của tôi ơi !"}]', '[{"time": "Mon Jan 09 02:31:58 ICT 2017", "userID": "13", "content": "hay quá hay quá"}, {"time": "Mon Jan 09 02:31:58 ICT 2017", "userID": "13", "content": "hay quá hay quá"}, {"time": "Mon Jan 09 02:34:17 ICT 2017", "userID": "13", "content": "Trông ngon quá nà"}, {"time": " Mon Jan 09 02:37:51 ICT 2017", "userID": "13", "content": "ahihi"}, {"time": " Mon Jan 09 02:37:58 ICT 2017", "userID": "13", "content": "hihi"}, {"time": " Mon Jan 09 02:38:15 ICT 2017", "userID": "13", "content": "moa moa"}, {"time": " Mon Jan 09 03:18:39 ICT 2017", "userID": "13", "content": "a"}, {"time": " Mon Jan 09 03:18:43 ICT 2017", "userID": "13", "content": "b"}, {"time": " Mon Jan 09 03:18:52 ICT 2017", "userID": "13", "content": "t"}, {"time": " Mon Jan 09 16:19:03 ICT 2017", "userID": "13", "content": "dfgdfg"}, {"time": " Mon Jan 09 16:19:35 ICT 2017", "userID": "13", "content": "sdgdsg"}, {"time": " Mon Jan 09 16:20:14 ICT 2017", "userID": "13", "content": "rye4dfhfdh"}, {"time": " Mon Jan 09 16:22:53 ICT 2017", "userID": "15", "content": "sdfdsg"}, {"time": " Mon Jan 09 16:29:55 ICT 2017", "userID": "17", "content": "sdsdgsfg"}, {"time": " Fri Jan 13 16:15:47 ICT 2017", "userID": "14", "content": "Hồ Anh."}, {"time": "5 tháng 0 lúc 16:40", "userID": "14", "content": "Anh"}, {"time": "3 tháng 1 lúc 16:41", "userID": "14", "content": "sdfdsfdsf"}, {"time": "5 tháng 0 lúc 16:43", "userID": "14", "content": "sdfdsf"}, {"time": "13 Jan 2017 09:44:49 GMT", "userID": "14", "content": "sdfsdf"}, {"time": "13 tháng 1 lúc 16:22", "userID": "14", "content": "ưerewr"}]', 'Ngon từ gạo, ngọt từ sườn, ăn là mê, ăn là mê :D', 0),
(2, 'Nem Chua', 1, '[2, 3, 4, 5, 6, 7]', 15000, 'https://blog.kitfe.com/wp-content/uploads/2016/07/nem-chua-TT-2.jpg', 13, NULL, '[{"time": " Mon Jan 09 02:45:07 ICT 2017", "userID": "15", "content": "chủ nhật tuần này đi ăn món này mới được !"}, {"time": " Mon Jan 09 02:54:27 ICT 2017", "userID": "13", "content": "Nice, mọi người nên ăn thử !"}]', '[{"time": "Mon Jan 09 01:52:50 ICT 2017", "userID": "13", "content": "Món này có cay lắm không nhỉ ?"}, {"time": "Mon Jan 09 01:52:50 ICT 2017", "userID": "13", "content": "Avatar xinh quá, thớt cho làm quen nhé :v, mình sẽ ăn thường xuyên !"}, {"time": "Mon Jan 09 01:52:50 ICT 2017", "userID": "14", "content": "Bạn chỉ rõ địa chỉ dùm mình được không, mình dẫn gấu đi ăn !"}, {"time": "Mon Jan 09 01:52:50 ICT 2017", "userID": "13", "content": "Tuyệt quá các bạn ơi, Sky ơi !!"}, {"time": "Mon Jan 09 01:52:50 ICT 2017", "userID": "14", "content": "Trông thèm quá, có khuyến mại không bạn ?"}, {"time": "Mon Jan 09 01:52:50 ICT 2017", "userID": "13", "content": "Ôi, LẠC TRÔI giữa đời lại có món ngon thế này ahihi :3"}, {"time": "Mon Jan 09 01:52:50 ICT 2017", "userID": "14", "content": "Ai mời tui ăn nài nài đi, tui làm BG ng đó, thưn lắm lun á !"}, {"time": " Mon Jan 09 02:39:52 ICT 2017", "userID": "13", "content": "Không biết Quảng cáo nem chua hay quảng cáo dầu ăn đây !"}, {"time": " Mon Jan 09 02:43:50 ICT 2017", "userID": "13", "content": "Không biết Quảng cáo nem chua hay quảng cáo dầu ăn đây !"}, {"time": " Mon Jan 09 03:16:57 ICT 2017", "userID": "14", "content": "Địa chỉ ở đâu vậy nhỉ ?"}, {"time": " Mon Jan 09 03:17:24 ICT 2017", "userID": "14", "content": "Nem chua Thanh Hóa hay nem gì v mọi người ?"}, {"time": " Mon Jan 09 03:22:35 ICT 2017", "userID": "13", "content": "Nem này nem Lào nha bạn :D !"}, {"time": " Mon Jan 09 03:23:07 ICT 2017", "userID": "14", "content": "Quỷ sứ hà :3 !!!"}, {"time": " Tue Jan 10 15:44:47 ICT 2017", "userID": "14", "content": "rtyru"}]', 'Nem chua Thanh Hóa chính hãng đây, mua 10 tặng 1, mua 11 tặng 2 đây, mại zô, mại zô...', 0),
(3, 'Cơm rang dưa bò', 1, '[5, 6, 7, 8, 2, 4]', 30000, 'http://naungon.com/images/nau.vn/wp-content/uploads/2014/12/com-rang-dua-bo.jpg', 19, NULL, '[{"time": " Mon Jan 09 02:45:07 ICT 2017", "userID": "15", "content": "chủ nhật tuần này đi ăn món này mới được !"}, {"time": " Mon Jan 09 02:54:27 ICT 2017", "userID": "13", "content": "Nice, mọi người nên ăn thử !"}]', '[{"time": " Mon Jan 09 02:45:07 ICT 2017", "userID": "15", "content": "Món ngon thế này phải dẫn bạn #Anh đi ăn mới được, ahihi :))"}, {"time": " Mon Jan 09 02:54:27 ICT 2017", "userID": "13", "content": "Đẹp trai nhưng không dễ dãi nha, Bao thì đi :))"}, {"time": " Mon Jan 09 16:03:27 ICT 2017", "userID": "15", "content": "Tớ bao 2 bạn nhé :v "}, {"time": " Tue Jan 10 02:53:03 ICT 2017", "userID": "13", "content": "(y)"}, {"time": " Tue Jan 10 15:44:30 ICT 2017", "userID": "14", "content": "hyyyyyyyyyyyyyyyyyyyyyyyy"}, {"time": " Tue Jan 10 15:44:35 ICT 2017", "userID": "14", "content": "yyyyyyyyyyy"}, {"time": " Tue Jan 10 15:44:53 ICT 2017", "userID": "14", "content": "tuy"}]', 'Cơm rang dưa bò, dưa ướp 7 7 49 ngày, thịt bò nuôi 4 3 12 năm, ngon lắm luông.', 0),
(5, 'Tomato - Hải Sản Tươi Sống', 2, '[1, 4, 5, 7, 8]', 300000, 'https://media.foody.vn/res/g5/46742/prof/s640x400/foody-mobile-hai-san-jpg-145-636195488583692545.jpg', 15, NULL, NULL, NULL, 'Hải sản tươi ngon, giá rẻ số 1', 0),
(8, 'Tomato - Hải Sản Tươi Sống', 2, '[1, 4, 5, 7, 8]', 300000, 'https://media.foody.vn/res/g5/46742/prof/s640x400/foody-mobile-hai-san-jpg-145-636195488583692545.jpg', 13, NULL, NULL, NULL, 'Hải sản tươi ngon, giá rẻ số 1', 1),
(9, 'Salmonoid - Món Âu - Cao Bá Quát', 4, '[1, 4, 5, 8]', 500000, 'https://media.foody.vn/res/g15/142863/prof/s480x300/foody-mobile-salmond-jpg-972-636137906617571606.jpg', 16, '[21]', '[{"time": "18 tháng 1 lúc 11:58", "userID": "21", "content": "Món ngon Hà Nội"}]', '[{"time": "18 tháng 1 lúc 11:11", "userID": "21", "content": "Nóm ngon !"}]', 'Mở cửa 10:00 AM - 10:30 PM', 0),
(10, 'Chả Cá Hà Thành - Nguyễn Văn Huyên', 5, '[1, 11, 12, 5]', 50000, 'https://media.foody.vn/res/g20/193590/prof/s480x300/foody-mobile-chaca960-jpg-511-635991603552230668.jpg', 17, NULL, NULL, NULL, 'Mở cửa 08:00 AM - 10:30 PM', 1),
(11, 'Vua Tào Phớ', 6, '[1, 2, 5, 11, 12]', 10000, 'https://media.foody.vn/res/g29/284403/prof/s640x400/foody-mobile-vtp-jpg-351-636120364212130808.jpg', 18, NULL, NULL, NULL, 'Ngon bổ rẻ', 1),
(12, 'GRILLE6 - Steakhouse', 7, '[1, 2, 4, 9, 13]', 50000, 'https://media.foody.vn/res/g7/68777/prof/s640x400/foody-mobile-gril-jpg-908-635990743421962398.jpg', 19, NULL, NULL, NULL, 'Mở cửa 08:30 AM - 10:00 PM', 1),
(13, 'Highlands Coffee', 8, '[1, 3, 11]', 60000, 'https://media.foody.vn/res/g1/6587/prof/s480x300/foody-mobile-hl1-jpg-164-635754939818433210.jpg', 19, NULL, NULL, NULL, 'Mở cửa 08:00 AM - 10:00 PM', 0),
(14, 'Bờm Quán - Ăn Vặt Đường Phố', 9, '[1, 11, 12]', 10000, 'https://media.foody.vn/res/g29/285778/prof/s480x300/foody-mobile-bom1-jpg-933-636125525510669536.jpg', 14, NULL, NULL, NULL, 'Mở cửa 08:00 AM - 09:30 PM', 1),
(15, 'Tuna\'s Fastfood & Drink', 10, '[1, 2, 3, 13]', 13000, 'https://media.foody.vn/res/g29/288243/prof/s480x300/foody-mobile-tuna-jpg-468-636138635834189800.jpg', 15, NULL, NULL, NULL, 'mở cửa 08:00 AM - 10:30 PM', 1),
(16, 'Neverland Food & Coffee', 11, '[1, 3, 9, 13]', 15000, 'https://media.foody.vn/res/g27/262225/prof/s480x300/foody-mobile-13895291_10364969331-920-636058390002853084.jpg', 16, NULL, NULL, NULL, 'mở cửa 08:00 AM - 10:00 PM', 0),
(17, 'The Lissom Parlour Cafeteria & Bakery', 12, '[1, 2, 6]', 39000, 'https://media.foody.vn/res/g21/206778/prof/s480x300/20161118115555-14055169_318322871850852_6928210734649138595_n.jpg', 17, NULL, NULL, NULL, 'mở cửa 08:30 AM - 10:30 PM', 0),
(18, 'Kisu Sushi', 13, '[1, 2, 9, 11]', 50000, 'https://media.foody.vn/res/g23/222775/prof/s480x300/foody-mobile-6-jpg-924-635947559116321294.jpg', 18, NULL, NULL, NULL, 'mở cửa 08:00 AM - 10:00 PM', 0),
(19, 'Gia Đình BB - Cơm Tấm Sài Gòn', 14, '[1, 4, 5]', 75000, 'https://media.foody.vn/res/g5/42942/prof/s480x300/foody-mobile-comtam-jpg-169-636147136925767684.jpg', 19, NULL, NULL, NULL, 'mở cửa Trưa: 10:30 AM - 02:30 PM | Tối: 05:30 PM - 10:00 PM', 1),
(20, 'Royaltea - Trà Sữa Hồng Kông', 15, '[1, 2, 3, 11]', 20000, 'https://media.foody.vn/res/g32/316219/prof/s480x300/foody-mobile-tra-jpg-248-636190591961865231.jpg', 13, NULL, NULL, NULL, 'mở cửa 07:30 AM - 10:00 PM', 0),
(26, 'Hotdog & Ăn Vặt', 26, '[1, 5, 6, 11, 12, 13]', 10000, 'https://media.foody.vn/res/g16/157407/prof/s480x300/foody-mobile-1-jpg-785-635743804482268724.jpg', 14, NULL, NULL, NULL, NULL, 1),
(27, 'Trà Sữa Bobapop', 27, '[1, 2, 3, 11, 12]', 10000, 'https://media.foody.vn/res/g32/314540/prof/s480x300/foody-mobile-2-jpg-862-636181000399617159.jpg', 15, NULL, NULL, NULL, NULL, 1),
(28, 'Hotdog & Ăn Vặt', 26, '[1, 5, 6, 11, 12, 13]', 10000, 'https://media.foody.vn/res/g16/157407/prof/s480x300/foody-mobile-1-jpg-785-635743804482268724.jpg', 16, NULL, NULL, NULL, NULL, 1),
(29, 'Trà Sữa Bobapop', 27, '[1, 2, 3, 11, 12]', 10000, 'https://media.foody.vn/res/g32/314540/prof/s480x300/foody-mobile-2-jpg-862-636181000399617159.jpg', 17, NULL, NULL, NULL, NULL, 1),
(30, 'Bún Đậu Mắm Tôm', 29, '[1, 11, 12]', 25000, 'https://media.foody.vn/res/g26/255586/prof/s640x400/foody-mobile-13567072_70009691012-564-636039994602805422.jpg', 18, NULL, NULL, NULL, NULL, 1),
(31, 'Takitimu Ice Cream', 30, '[1, 11, 12]', 10000, 'https://media.foody.vn/res/g23/223552/prof/s480x300/foody-mobile-taki960-jpg-971-635980526890931199.jpg', 19, NULL, NULL, NULL, NULL, 1),
(32, 'Feeling Tea', 31, '[1, 2, 3, 11, 12]', 15000, 'https://media.foody.vn/res/g8/72087/prof/s480x300/foody-mobile-ft-jpg-312-635768735721744217.jpg', 13, NULL, NULL, NULL, NULL, 1),
(33, 'Ding Tea', 32, '[1, 2, 3, 11, 12]', 20000, 'https://media.foody.vn/res/g29/284486/prof/s640x400/foody-mobile-2-jpg-461-636120352652738232.jpg', 14, NULL, NULL, NULL, NULL, 1),
(34, 'Chè ba miền', 33, '[1, 11, 12]', 13000, 'https://media.foody.vn/res/g7/60702/prof/s480x300/foody-mobile-che-jpg-706-636143739994252965.jpg', 15, NULL, NULL, NULL, NULL, 1),
(35, 'Súp Gà, Mì Gà & Gà Tần', 34, '[1, 2, 5, 6]', 30000, 'https://media.foody.vn/res/g32/314899/prof/s480x300/foody-mobile-2-jpg-945-636184434519441594.jpg', 16, NULL, NULL, NULL, NULL, 1),
(36, 'Bánh Đúc Nóng', 35, '[1, 11, 12]', 20000, 'https://media.foody.vn/res/g1/7986/prof/s480x300/foody-mobile-banh-duc1-jpg-131-635850843423052258.jpg', 17, NULL, NULL, NULL, NULL, 1),
(37, 'Bún Bò Huế O Huyền', 36, '[1, 2, 12, 11]', 20000, 'https://media.foody.vn/res/g21/201483/prof/s480x300/foody-mobile-bunbohue3-jpg-436-635875124010329843.jpg', 18, NULL, NULL, NULL, NULL, 1),
(38, 'Bánh Đúc Nóng', 35, '[1, 11, 12]', 20000, 'https://media.foody.vn/res/g1/7986/prof/s480x300/foody-mobile-banh-duc1-jpg-131-635850843423052258.jpg', 19, NULL, NULL, NULL, NULL, 1),
(39, 'Bún Bò Huế O Huyền', 36, '[1, 2, 12, 11]', 20000, 'https://media.foody.vn/res/g21/201483/prof/s480x300/foody-mobile-bunbohue3-jpg-436-635875124010329843.jpg', 13, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `foodclass`
--

CREATE TABLE `foodclass` (
  `classcode` int(11) NOT NULL,
  `classname` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `foodclass`
--

INSERT INTO `foodclass` (`classcode`, `classname`) VALUES
(1, 'Tất cả'),
(2, 'Ăn với người yêu'),
(3, 'Góc cà phê'),
(4, 'Ăn với gia đình'),
(5, 'Ăn với nhóm'),
(6, 'Tiệm bánh'),
(7, 'Hải sản'),
(8, 'Nướng & Lẩu'),
(9, 'Nhật Bản'),
(10, 'Pizza'),
(11, 'Ăn vặt & Vỉa hè'),
(12, 'Ăn một mình'),
(13, 'Hàn Quốc');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(225) NOT NULL,
  `name` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passWord` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlPicture` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idoftype` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `passWord`, `email`, `gender`, `urlPicture`, `type`, `idoftype`) VALUES
(13, 'Anh Hồ', NULL, 'hoangtubanggia_0708super@yahoo.com', 'Nam', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/14991878_1065715086870827_1626852401711531930_n.jpg?oh=cfbfb002d78ffdf1ddce524443ea51cb&oe=58DCBEB8', 'facebook', '1045448088897527'),
(14, 'Anh Ciber', NULL, 'hovananhk58a2@gmail.com', 'Nam', 'https://lh6.googleusercontent.com/-vqYzWfT28Ts/AAAAAAAAAAI/AAAAAAAAAKc/OCEjeKVndYI/photo.jpg', 'google', '109681741866290846134'),
(15, 'Thoa Phạm', NULL, '', 'Nữ', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/15894935_1764040260583223_9038904536624285689_n.jpg?oh=2407b25348e59992ff1bf24772dedf68&oe=5914C8F4', 'facebook', '1764988230488426'),
(16, 'Dương', NULL, 'duongsilver1995@gmail.com', 'Nam', 'https://lh4.googleusercontent.com/-LbvXg4uKzV0/AAAAAAAAAAI/AAAAAAAAABE/h5DNTc5SDuM/photo.jpg', 'google', '106365768746173501122'),
(17, 'Văn Anh', 'c138539441e4fc32b43601c837453f9915003243aad6db9d3da72a86cdd46583', 'hoanh@gmail.com', 'Nam', 'https://scontent.fhan3-1.fna.fbcdn.net/v/t1.0-9/14590372_603767893129596_7463257834055002695_n.jpg?oh=a1c3eecd9b3fc3bcb1c8ab66e505892f&oe=591669DF', 'normal', ''),
(18, 'Ahihi', '8fcb25879a66b007def5cf63d7960fdfa2ee1f2b056d9e8e6008e7873a7af202', 'ahihi@gmail.com', 'Nam', 'https://scontent.fhan3-1.fna.fbcdn.net/v/t1.0-9/14590372_603767893129596_7463257834055002695_n.jpg?oh=a1c3eecd9b3fc3bcb1c8ab66e505892f&oe=591669DF', 'normal', ''),
(19, 'Hoàn Kì', NULL, 'hoanki2212@gmail.com', 'Nam', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/10458664_278390079000714_1030901753428749125_n.jpg?oh=16b6b4110db9e4f975858faca2975cfd&oe=590E37F9', 'facebook', '618345075005211'),
(20, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'admin@gmail.com', 'Nam', 'https://scontent.fhan3-1.fna.fbcdn.net/v/t1.0-9/14590372_603767893129596_7463257834055002695_n.jpg?oh=a1c3eecd9b3fc3bcb1c8ab66e505892f&oe=591669DF', 'admin', ''),
(21, 'Hoàn', '6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918', 'hoan@gmail.com', 'Nam', 'https://scontent.fhan3-1.fna.fbcdn.net/v/t1.0-9/14590372_603767893129596_7463257834055002695_n.jpg?oh=a1c3eecd9b3fc3bcb1c8ab66e505892f&oe=591669DF', 'normal', ''),
(22, 'Hoàn Kì', '', 'hoanki2212@gmail.com', 'Nam', 'https://lh4.googleusercontent.com/-TwezwcsysY8/AAAAAAAAAAI/AAAAAAAAAwY/VuQPR4tPW7Y/photo.jpg', 'google', '101387877056508318441'),
(23, 'Hoan hihi', '86e50149658661312a9e0b35558d84f6c6d3da797f552a9657fe0558ca40cdef', '34@gmail.com', 'Nam', 'https://scontent.fhan3-1.fna.fbcdn.net/v/t1.0-9/14590372_603767893129596_7463257834055002695_n.jpg?oh=a1c3eecd9b3fc3bcb1c8ab66e505892f&oe=591669DF', 'normal', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`foodcode`);

--
-- Indexes for table `foodclass`
--
ALTER TABLE `foodclass`
  ADD PRIMARY KEY (`classcode`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `foodcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `foodclass`
--
ALTER TABLE `foodclass`
  MODIFY `classcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
