package com.food.foodQuery;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.food.db.DBConnect;
import com.food.table.Food;

public class FoodQuery {

	public FoodQuery() {

	}

	public ArrayList<Food> getFoodNotCheck() {
		DBConnect connect = new DBConnect();
		String sql = "SELECT * FROM food WHERE checked = '0'";
		ResultSet res = connect.getRs(sql);
		try {
			if (res.next() == false) {
				connect.closeCon();
				return null;
			}

			ArrayList<Food> ar = new ArrayList<>();
			int id = 0;
			int userId = 0;
			String imageurl = "";
			String description = "";

			while (res.next()) {
				id = res.getInt("foodcode");
				userId = res.getInt("userid");
				imageurl = res.getString("imageurl");
				description = res.getString("description");
				Food food = new Food();
				food.setFoodCode(id);
				food.setUserId(userId);
				food.setImageUrl(imageurl);
				food.setDescription(description);
				ar.add(food);
			}

			connect.closeCon();
			return ar;
		} catch (SQLException e) {

		}
		connect.closeCon();
		return null;
	}

	public ArrayList<Food> getFoodFromUserId(String userId) {
		DBConnect connect = new DBConnect();
		String sql = "SELECT * FROM food WHERE userid = '" + userId + "'";
		ResultSet res = connect.getRs(sql);
		try {
			if (res.next() == false) {
				connect.closeCon();
				return null;
			}

			ArrayList<Food> ar = new ArrayList<>();
			int id = 0;
			String imageurl = "";
			String description = "";

			while (res.next()) {
				
				id = res.getInt("foodcode");
				imageurl = res.getString("imageurl");
				description = res.getString("description");
				Food food = new Food();
				food.setFoodCode(id);
				food.setImageUrl(imageurl);
				food.setDescription(description);
				ar.add(food);
			}

			connect.closeCon();
			return ar;
		} catch (SQLException e) {

		}
		connect.closeCon();
		return null;
	}
	
	
	public ArrayList<Food> getFoodBestLike(String userId) {
		DBConnect connect = new DBConnect();
		String sql = "SELECT * FROM food WHERE userid = '" + userId + "'";
		ResultSet res = connect.getRs(sql);
		try {
			if (res.next() == false) {
				connect.closeCon();
				return null;
			}

			ArrayList<Food> ar = new ArrayList<>();
			int id = 0;
			String imageurl = "";
			String description = "";

			while (res.next()) {
				
				id = res.getInt("foodcode");
				imageurl = res.getString("imageurl");
				description = res.getString("description");
				Food food = new Food();
				food.setFoodCode(id);
				food.setImageUrl(imageurl);
				food.setDescription(description);
				ar.add(food);
			}

			connect.closeCon();
			return ar;
		} catch (SQLException e) {

		}
		connect.closeCon();
		return null;
	}
}
