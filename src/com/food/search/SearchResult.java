package com.food.search;

import java.util.ArrayList;

import com.food.table.Food;
import com.food.user.User;

public class SearchResult {
	private ArrayList<Food> foods;
	private ArrayList<User> users;

	public ArrayList<Food> getFoods() {
		return foods;
	}

	public void setFoods(ArrayList<Food> foods) {
		this.foods = foods;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	public SearchResult(ArrayList<Food> foods, ArrayList<User> users) {
		super();
		this.foods = foods;
		this.users = users;
	}

	public SearchResult() {
		super();
		// TODO Auto-generated constructor stub
		foods = new ArrayList<Food>();
		users = new ArrayList<User>();
	}

}
