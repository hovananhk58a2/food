package com.food.search;

public class TranferString {
	public String tranfer(String in) {
		String rs = "";
		String filter1 = earseTab(in);
		String cuts[] = filter1.split(" ");
		for (int i = 0; i < cuts.length; i++) {
			String temp = nomalize(cuts[i]);
			rs += temp;
		}
		rs = rs.replaceAll(" ", "");
		return rs;
	}

	private String earseTab(String in) {
		String rs = "";
		for (int i = 0; i < in.length(); i++) {
			if (in.charAt(i) == '	') {
				rs += " ";
			} else {	
				rs += in.charAt(i);
			}
		}
		return rs;
	}

	public String nomalize(String in) {
		String rs = "";
		String inn = in.toLowerCase();
		for (int i = 0; i < inn.length(); i++) {
			rs += fix(inn.charAt(i));
		}
		rs.replaceAll(" ", "");
		return rs;
	}

	public char fix(char c) {
		if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
			return c;
		else if (c == 'à' || c == 'á' || c == 'ả' || c == 'ã' || c == 'ạ')
			return 'a';
		else if (c == 'ă' || c == 'ằ' || c == 'ắ' || c == 'ẳ' || c == 'ẵ' || c == 'ặ')
			return 'a';
		else if (c == 'â' || c == 'ầ' || c == 'ấ' || c == 'ẩ' || c == 'ẫ' || c == 'ậ')
			return 'a';
		else if (c == 'đ')
			return 'd';
		else if (c == 'è' || c == 'é' || c == 'ẻ' || c == 'ẽ' || c == 'ẹ')
			return 'e';
		else if (c == 'ê' || c == 'ề' || c == 'ế' || c == 'ể' || c == 'ễ' || c == 'ệ')
			return 'e';
		else if (c == 'ì' || c == 'í' || c == 'ỉ' || c == 'ĩ' || c == 'ị')
			return 'i';
		else if (c == 'ò' || c == 'ó' || c == 'ỏ' || c == 'õ' || c == 'ọ')
			return 'o';
		else if (c == 'ô' || c == 'ồ' || c == 'ố' || c == 'ổ' || c == 'ỗ' || c == 'ộ')
			return 'o';
		else if (c == 'ơ' || c == 'ờ' || c == 'ớ' || c == 'ở' || c == 'ỡ' || c == 'ợ')
			return 'o';
		else if (c == 'ù' || c == 'ú' || c == 'ủ' || c == 'ũ' || c == 'ụ')
			return 'u';
		else if (c == 'ư' || c == 'ừ' || c == 'ứ' || c == 'ử' || c == 'ữ' || c == 'ự')
			return 'u';
		return ' ';
	}
}