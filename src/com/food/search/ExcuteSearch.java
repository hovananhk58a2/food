package com.food.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

import com.food.address.AddressQuery;
import com.food.db.DBConnect;
import com.food.query.FoodQuery;
import com.food.table.Food;
import com.food.user.User;
import com.food.user.UserQuery;

public class ExcuteSearch {
	public SearchResult search(String content) {
		SearchResult result = new SearchResult();
		content = new TranferString().tranfer(content);
		System.out.println(content);
		String sql = "select * from food";
		ArrayList<Food> foods = new ArrayList<>();
		DBConnect connect = new DBConnect();
		ResultSet rs = connect.getRs(sql);
		ArrayList<Integer> foodSet = new ArrayList<Integer>();
		try {
			while (rs.next()) {
				int id = rs.getInt("foodcode");
				if (!foodSet.contains(id)) {
					String name = rs.getString("foodname");
					String price = String.valueOf(rs.getInt("price"));
					String address = new AddressQuery().getAddressByID("" + rs.getInt("foodaddress")).getName();

					name = new TranferString().tranfer(name);
					price = new TranferString().tranfer(price);
					address = new TranferString().tranfer(address);

					if (name.contains(content)) {
						foods.add(new FoodQuery().getFoodsByID(id));
						foodSet.add(id);
					} else if (price.contains(content)) {
						foods.add(new FoodQuery().getFoodsByID(id));
						foodSet.add(id);
					} else if (address.contains(content)) {
						foods.add(new FoodQuery().getFoodsByID(id));
						foodSet.add(id);
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		result.setFoods(foods);
		sql = "select * from user";
		rs = connect.getRs(sql);
		ArrayList<User> users = new ArrayList<User>();
		ArrayList<Integer> userSet = new ArrayList<Integer>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				if (!userSet.contains(id)) {
					String name = rs.getString("name");
					String email = rs.getString("email");

					name = new TranferString().tranfer(name);
					email = new TranferString().tranfer(email);
					if(name.contains(content)){
						users.add(new UserQuery().getUserByID(id));
						userSet.add(id);
					}else if(email.contains(content)){
						users.add(new UserQuery().getUserByID(id));
						userSet.add(id);
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		result.setUsers(users);
		return result;
	}
}
