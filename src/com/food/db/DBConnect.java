package com.food.db;

import java.sql.*;

public class DBConnect {
	private Connection con;
	private Statement st;

	public DBConnect() {
		con = null;
		st = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// Local
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/web2016?autoReconnect=true&useSSL=false&setUnicode=true&characterEncoding=UTF-8",
					"root", "root");

			// Host
			/*con = DriverManager.getConnection(
					"jdbc:mysql://mysql116905-husfood.jelastic.regruhosting.ru/web2016?autoReconnect=true&useSSL=false",
					"root", "VIIxob39222");*/
			st = con.createStatement();
			
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}
	}

	public Connection getCon() {
		return con;
	}

	public ResultSet getRs(String sql) {
		ResultSet rs = null;
		try {
			rs = st.executeQuery(sql);
			
		} catch (Exception e) {
			System.err.println("Error2: " + e);
		}
		return rs;
	}

	public void executeUpdate(String sql) {
		try {
			st.executeUpdate(sql);
			
		} catch (Exception e) {
			System.err.println("Error3: " + e);
		}
	}

	public void closeCon() {
		if (con != null)
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
