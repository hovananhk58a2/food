package com.food.address;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.food.db.DBConnect;

public class AddressQuery {

	public AddressQuery() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address getAddressByID(String ID) {
		Address address = new Address();
		String sql = "select * from address where id=" + ID;
		DBConnect connect = new DBConnect();
		ResultSet rs = connect.getRs(sql);
		try {
			if (rs.next()) {
				address.setId(rs.getInt("id"));
				address.setName(rs.getString("name"));
				address.setIsPopular(rs.getInt("isPopular"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return address;
	}
}
