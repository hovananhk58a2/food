package com.food.address;

public class Address {
	private int id;
	private String name;
	private int isPopular;

	public Address(int id, String name, int isPopular) {
		super();
		this.id = id;
		this.name = name;
		this.isPopular = isPopular;
	}

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIsPopular() {
		return isPopular;
	}

	public void setIsPopular(int isPopular) {
		this.isPopular = isPopular;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", name=" + name + ", isPopular=" + isPopular + "]";
	}

}
