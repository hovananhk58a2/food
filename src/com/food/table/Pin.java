package com.food.table;

public class Pin {
	private String userID;
	private String dateTime;
	private String content;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Pin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Pin(String userID, String dateTime, String content) {
		super();
		this.userID = userID;
		this.dateTime = dateTime;
		this.content = content;
	}

	@Override
	public String toString() {
		return "Pin [userID=" + userID + ", dateTime=" + dateTime + ", content=" + content + "]";
	}

}
