package com.food.table;

import java.util.ArrayList;

public class Food {
	private int foodCode;
	private String foodName;
	private String foodAddress;
	private ArrayList<Integer> foodclass;
	private String imageUrl;
	private int userId;
	private ArrayList<Integer> likes;
	private ArrayList<Pin> pins;
	private ArrayList<Comment> comments;
	private String description;
	private int price;
	private int checked;

	public int getChecked() {
		return checked;
	}

	public void setChecked(int checked) {
		this.checked = checked;
	}

	public Food(int foodCode, String foodName, String foodAddress, ArrayList<Integer> foodclass, String imageUrl,
			int userId, ArrayList<Integer> likes, ArrayList<Pin> pins, ArrayList<Comment> comments, String description,
			int price, int checked) {
		super();
		this.foodCode = foodCode;
		this.foodName = foodName;
		this.foodAddress = foodAddress;
		this.foodclass = foodclass;
		this.imageUrl = imageUrl;
		this.userId = userId;
		this.likes = likes;
		this.pins = pins;
		this.comments = comments;
		this.description = description;
		this.price = price;
		this.checked = checked;
	}

	public String getPrice() {
		String rs = "";
		String pc = "" + price;
		int cnt = 0;
		for (int i = pc.length() - 1; i >= 0; i--) {
			cnt++;
			rs += pc.charAt(i);
			if (cnt % 3 == 0 && cnt != pc.length()) {
				rs += ",";
			}
		}
		String rs2 = "";
		for (int i = rs.length() - 1; i >= 0; i--) {
			rs2 += rs.charAt(i);
		}
		return rs2;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Food() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getFoodCode() {
		return foodCode;
	}

	public void setFoodCode(int foodCode) {
		this.foodCode = foodCode;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public String getFoodAddress() {
		return foodAddress;
	}

	public void setFoodAddress(String foodAddress) {
		this.foodAddress = foodAddress;
	}

	public ArrayList<Integer> getFoodclass() {
		return foodclass;
	}

	public void setFoodclass(ArrayList<Integer> foodclass) {
		this.foodclass = foodclass;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ArrayList<Integer> getLikes() {
		return likes;
	}

	public void setLikes(ArrayList<Integer> likes) {
		this.likes = likes;
	}

	public ArrayList<Pin> getPins() {
		return pins;
	}

	public void setPins(ArrayList<Pin> pins) {
		this.pins = pins;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Food [foodCode=" + foodCode + ", foodName=" + foodName + ", foodAddress=" + foodAddress + ", foodclass="
				+ foodclass + ", imageUrl=" + imageUrl + ", userId=" + userId + ", likes=" + likes + ", pins=" + pins
				+ ", comments=" + comments + ", description=" + description + ", price=" + price + "]";
	}

}
