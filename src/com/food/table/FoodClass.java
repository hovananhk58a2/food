package com.food.table;

public class FoodClass {
	private int classCode;
	private String className;

	public FoodClass() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FoodClass(int classCode, String className) {
		super();
		this.classCode = classCode;
		this.className = className;
	}

	public int getClassCode() {
		return classCode;
	}

	public void setClassCode(int classCode) {
		this.classCode = classCode;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	public String toString() {
		return "FoodClass [classCode=" + classCode + ", className=" + className + "]";
	}

}
