package com.food.table;

public class Comment {
	private String userID;
	private String dateTime;
	private String content;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Comment [userID=" + userID + ", dateTime=" + dateTime + ", content=" + content + "]";
	}

	public Comment(String userID, String dateTime, String content) {
		super();
		this.userID = userID;
		this.dateTime = dateTime;
		this.content = content;
	}

	public Comment() {
		super();
		// TODO Auto-generated constructor stub
	}

}
