package com.food.query;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.food.db.DBConnect;
import com.food.table.Comment;
import com.food.table.Pin;

public class SocialQuery {
	public SocialQuery() {
		// TODO Auto-generated constructor stub
	}

	public void updateComments(int foodID, ArrayList<Comment> coms) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < coms.size(); i++) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("time", coms.get(i).getDateTime());
				jsonObject.put("userID", coms.get(i).getUserID());
				jsonObject.put("content", coms.get(i).getContent());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonArray.put(jsonObject);
		}
		String sql = "update food set comments ='" + jsonArray.toString() + "' where foodcode = " + foodID;
		System.out.println(sql);
		DBConnect connect = new DBConnect();
		connect.executeUpdate(sql);
		connect.closeCon();
		return;
	}
	
	public void updatePins(int foodID, ArrayList<Pin> pins){
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < pins.size(); i++) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("time", pins.get(i).getDateTime());
				jsonObject.put("userID", pins.get(i).getUserID());
				jsonObject.put("content", pins.get(i).getContent());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonArray.put(jsonObject);
		}
		String sql = "update food set pins ='" + jsonArray.toString() + "' where foodcode = " + foodID;
		System.out.println(sql);
		DBConnect connect = new DBConnect();
		connect.executeUpdate(sql);
		connect.closeCon();
		return;
	}
	
	public void updateLikes(int foodID, ArrayList<Integer> likes){
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < likes.size(); i++) {
			jsonArray.put(likes.get(i));
		}
		System.out.println("likes: "+jsonArray.toString());
		String sql = "update food set likes ='" + jsonArray.toString() + "' where foodcode = " + foodID;
		System.out.println(sql);
		DBConnect connect = new DBConnect();
		connect.executeUpdate(sql);
		connect.closeCon();
		return;
	}
}
