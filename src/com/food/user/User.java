package com.food.user;

public class User {

	private int id = 0;
	private String name = "";
	private String passWord = "";
	private String email = "";
	private String gender = "";
	private String urlPicture = "";
	private String type = ""; // admin, normal, facebook, google.
	private String idoftype = ""; // for facebook or google

	public User(int id, String name, String passWord, String email, String gender, String urlPicture, String type,
			String idoftype) {
		super();
		this.id = id;
		this.name = name;
		this.passWord = passWord;
		this.email = email;
		this.gender = gender;
		this.urlPicture = urlPicture;
		this.type = type;
		this.idoftype = idoftype;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUrlPicture() {
		return urlPicture;
	}

	public void setUrlPicture(String urlPicture) {
		this.urlPicture = urlPicture;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdoftype() {
		return idoftype;
	}

	public void setIdoftype(String idoftype) {
		this.idoftype = idoftype;
	}

	public String toJsonString() {
		String s = "{\"id\":\"" + idoftype + "\",\"name\":\"" + name + "\",\"email\":\"" + email + "\",\"picture\":\""
				+ urlPicture + "\",\"gender\":\"" + gender + "\"}";
		return s;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", passWord=" + passWord + ", email=" + email + ", gender="
				+ gender + ", urlPicture=" + urlPicture + ", type=" + type + ", idoftype=" + idoftype + "]";
	}

}
