package com.food.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.food.db.DBConnect;
import com.mysql.jdbc.DatabaseMetaData;

public class UserQuery {
	public UserQuery() {

	}

	public User getUserByID(int ID) {
		User user = new User();
		DBConnect connect = new DBConnect();
		String sql = "select * from user where id=" + ID;
		ResultSet rs = connect.getRs(sql);
		try {
			if (rs.next()) {
				user.setName(rs.getString("name"));
				user.setId(ID);
				user.setEmail(rs.getString("email"));
				user.setGender(rs.getString("gender"));
				user.setUrlPicture(rs.getString("urlPicture"));
				user.setType(rs.getString("type"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return user;
	}

	public User getUserByEmailAndIdoftype(String email, String idoftye) {
		User user = new User();
		DBConnect connect = new DBConnect();
		String sql = "select * from user where email='" + email + "' and idoftype='" + idoftye + "'";
		System.out.println(sql);
		ResultSet rs = connect.getRs(sql);
		try {
			if (rs.next()) {
				user.setName(rs.getString("name"));
				user.setId(rs.getInt("id"));
				user.setEmail(rs.getString("email"));
				user.setGender(rs.getString("gender"));
				user.setUrlPicture(rs.getString("urlPicture"));
				user.setType(rs.getString("type"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return user;
	}

	public User getUserByIdOfType(String type, String idoftye) {
		User user = new User();
		DBConnect connect = new DBConnect();
		String sql = "select * from user where type='" + type + "' and idoftype='" + idoftye + "'";
		ResultSet rs = connect.getRs(sql);
		try {
			if (rs.next()) {
				user.setName(rs.getString("name"));
				user.setId(rs.getInt("id"));
				user.setEmail(rs.getString("email"));
				user.setGender(rs.getString("gender"));
				user.setUrlPicture(rs.getString("urlPicture"));
				return user;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return null;
	}

	public User getUserByEmailAndPassWord(String email, String passWord) {
		DBConnect connect = new DBConnect();
		String sql = "SELECT * FROM user WHERE email= '" + email + "' AND passWord = '" + passWord
				+ "' AND ( type = 'admin' OR type = 'normal' )";
		ResultSet rs = connect.getRs(sql);
		try {
			while (rs.next()) {
				User userFromDb = new User();
				userFromDb.setId(rs.getInt("id"));
				userFromDb.setEmail(rs.getString("email"));
				userFromDb.setName(rs.getString("name"));
				userFromDb.setGender(rs.getString("gender"));
				userFromDb.setUrlPicture(rs.getString("urlPicture"));
				userFromDb.setIdoftype(rs.getString("idoftype"));
				userFromDb.setType(rs.getString("type"));
				userFromDb.setPassWord(rs.getString("passWord"));
				connect.closeCon();
				return userFromDb;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return null;
	}

	public int getPublicNums(int userID) {
		int cnt = 0;
		String sql = "SELECT COUNT(*) FROM food where userid=" + userID;
		DBConnect connect = new DBConnect();
		ResultSet rs = connect.getRs(sql);
		try {
			if (rs.next()) {
				cnt = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return cnt;
	}

	// If table does not exist, create it
	public void checkTableExist(String s) {
		try {
			DBConnect connect = new DBConnect();
			DatabaseMetaData dbm = (DatabaseMetaData) connect.getCon().getMetaData();
			ResultSet tables = dbm.getTables(null, null, s, null);
			// Table does not exist
			if (tables.next() == false) {

				String commandCreateTable = "CREATE TABLE " + s
						+ " (id int(225) NOT NULL AUTO_INCREMENT, name text(1000),passWord varchar(1000), email varchar(1000), gender varchar(1000), urlPicture varchar(5000), type varchar(1000), idoftype varchar(1000), PRIMARY KEY (id) )";
				connect.executeUpdate(commandCreateTable);
			}
			connect.closeCon();
		} catch (Exception e) {
		}
	}

	public void saveUserToDatabase(User user) {
		String table = "user";

		try {
			DBConnect connect = new DBConnect();
			int userID = 0;

			String commandGetNextId = "SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'web2016' AND TABLE_NAME = 'user'";
			ResultSet res = new DBConnect().getRs(commandGetNextId);
			if (res.next()) {
				userID = res.getInt("AUTO_INCREMENT");
			}

			String commandInsertUser = "INSERT INTO " + table
					+ " ( name , passWord, email , gender , type , idoftype, urlPicture) VALUES ( '" + user.getName()
					+ "','" + user.getPassWord() + "','" + user.getEmail() + "','" + user.getGender() + "','"
					+ user.getType() + "','" + user.getIdoftype() + "','" + user.getUrlPicture() + "')";
			connect.executeUpdate(commandInsertUser);
			user.setId(userID);
			connect.closeCon();
		} catch (Exception e) {
			System.out.println("Error save user to database");
		}

	}

	public boolean checkUserByEmail(String email) {

		try {
			DBConnect connect = new DBConnect();
			String sql = "select * from user where email='" + email + "'";
			ResultSet rs = connect.getRs(sql);
			if (rs.next()) {
				connect.closeCon();
				return true;
			}
			connect.closeCon();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return false;
		}

		return false;
	}

	public String getUrlPictureById(String id) {
		try {
			DBConnect connect = new DBConnect();
			String sql = "SELECT urlPicture FROM user WHERE id= '" + id + "'";
			ResultSet res = connect.getRs(sql);
			while (res.next()) {

				return res.getString("urlPicture");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}
		return null;
	}

	public String getNameById(String id) {

		try {
			DBConnect connect = new DBConnect();
			String sql = "SELECT name FROM user WHERE id= '" + id + "'";
			ResultSet res = connect.getRs(sql);
			while (res.next()) {
				return res.getString("name");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}
		return null;
	}

}
